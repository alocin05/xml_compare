#!/usr/bin/python
import sys
import os
from hashlib import sha512
from lxml import etree
from copy import deepcopy

missing_el = []

'''XML COMPARE'''
def compare_tree(root1, root2):
    '''at the moment it doesn't check the root element'''
    # if root1.attrib and root2.attrib:
    #     if root1.tag == root2.tag and dict_compare(root1.attrib, root2.attrib):
    #         pass
    #     else:
    #         if root1.tag == root2.tag:
    #             pass
    #         else:
    #             #put it in list #IT'S DIFFERENT
    #             print '[-]', root1.tag

    if list(root1) and list(root2):

        for e1 in root1:
            flag = False
            buff = None
            for e2 in root2:
                if e1.tag == e2.tag:
                    if e1.attrib:
                        if e2.attrib:
                            if dict_compare(e1.attrib, e2.attrib):
                                if e1.text and e2.text:
                                    if e1.text == e2.text:     #CASE: tag + attribute + text
                                        flag = True
                                        buff = e2
                                        break
                                flag = True             #CASE: tag + attribute
                                buff = e2
                                break
                    elif e1.text and e2.text:
                        if e1.text == e2.text:      #CASE: tag + text
                            flag = True
                            buff = e2
                            break
                    else:                           #CASE: tag
                        flag = True
                        buff = e2
                        break
                else:
                    flag = False
            if flag == True:
                # print '[+]', e1.tag
                pass
            else:
                #missing_el mandatory global variable
                global missing_el
                missing_el.append(str(e1.tag) + ' ' + str(e1.attrib))
                # print '[-]', e1.tag, e1.attrib, e1.text

            if buff != None:
                compare_tree(e1, buff)

def dict_compare(d1, d2):
    ''' INPUT: 2 dict
    OUTPUT: boolean
    True even if the string value is in different order e.g. 'ab xy' 'xy ab' '''

    flag = False
    for a1, b1 in d1.iteritems():
        if a1 in d2:
            if b1 == d2[a1]:                #CASE: attribute are exactly the same
                # print '[+]', a1, '-', b1, 'tag present'
                flag = True
            else:                           #CASE: attribute element in different order separated by space
                lst1 = b1.split()
                lst2 = d2[a1].split()

                f = False
                for i in lst1:              #CASE: attribute="xyz abx" = attribute="abc xyz"
                    if i in lst2:
                        f = True
                    else:
                        f = False
                        break
                if f:
                    # print '[+]', a1, '-', b1, 'tag present BUT in different order'
                    flag = True
                else:
                    # print '[-]', a1, 'is present but', b1, 'is DIFFERENT'
                    flag = False
                    break
        else:
            # print '[-]', a1, 'is NOT inside'
            flag = False
            break
    return flag

def compare_xml(file1, file2):
    ''' INPUT: 2 strs, source of the xml files
    OUTPUT: 2 lsts of missing elements'''

    tree1 = etree.parse(file1)
    tree2 = etree.parse(file2)
    root1 = tree1.getroot()
    root2 = tree2.getroot()

    print 'Comparing ' + file1 + ' files...'
    compare_tree(root1, root2)
    global missing_el
    man1 = deepcopy(missing_el)
    del missing_el[:]   #clear the global list
    compare_tree(root2, root1)
    # print 'Done.'

    return (man1, missing_el)

def main():
    if len(sys.argv) != 3:
        print '\nERROR: invalid number of arguments.'
        print 'It compares all the .zip files with the same name in two different input directories'
        print 'Usage: python xml_compare.py [first/xml/file/path] [second/xml/file/path]\n'
        print 'e.g. python xml_compare.py ~/Desktop/file_to_test.xml ~/Desktop/oldxml/file_to_test.xml'
        sys.exit(0)
    else:
        source1 = sys.argv[1]
        source2 = sys.argv[2]

        hasher1 = sha512()
        with open(source1, 'rb') as afile:
            buf = afile.read()
            hasher1.update(buf)
        hash1 = hasher1.hexdigest()

        hasher2 = sha512()
        with open(source2, 'rb') as afile:
            buf = afile.read()
            hasher2.update(buf)
        hash2 = hasher2.hexdigest()
        if hash1 == hash2:
            print '\t files are identical.'
        else:
            man1, man2 = compare_xml(source1, source2)
            if man1 or man2:
                for i in man1:
                    print i
                print '----------'
                for i in man2:
                    print i
            else:
                print '\t files are equal but not identical.'

if __name__ == '__main__':
    main()
